<?php
session_start();
include 'koneksi.php';

if (isset($_POST['login'])) {


	$cek = mysqli_query($conn, "SELECT * FROM tb_admin WHERE username = '" . htmlspecialchars($_POST['user']) . "' AND password ='" . MD5($_POST['pass']) . "' ");

	if (mysqli_num_rows($cek) > 0) {
		$a = mysqli_fetch_object($cek);

		$_SESSION['stat_login'] = true;
		$_SESSION['id'] = $a->id_admin;
		$_SESSION['nama'] = $a->nm_admin;
		echo '<script>window.location="beranda.php"</script>';
	} else {
		echo '<script>alert("Gagal, username atau password salah")</script>';
	}
}
?>

<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="stylesheet" type="text/css" href="css/log.css">

	<title>Login</title>
</head>

<body>
	<div class="container">
		<form action="" method="POST" class="login-email">
			<p class="login-text" style="font-size: 2rem; font-weight: 800;">Login</p>
			<div class="input-group">
				<input type="text" name="user">
			</div>
			<div class="input-group">
				<input type="password" name="pass">
			</div>
			<div class="input-group">
				<button type="submit" name="login" class="btn">Login</button>
			</div>
		</form>
	</div>
</body>

</html>
