<!DOCTYPE html>
<html lang="en">

<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Selamat Datang Di PSB Online</title>
  
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css" />

  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@600;700&display=swap" rel="stylesheet" />

  <link rel="stylesheet" href="css/dashboard.css" />
</head>

<body>
  <header>
    <nav>
      <img src="rdd.png" width="50">
      <i class="fas fa-bars" id="ham-menu"></i>
      <ul id="nav-bar">
        <li>
          <a href="login.php">Login</a>
        </li>
        <li>
          <a href="about.php">About</a>
        </li>
        <a href="contact.php">Contact</a>
        </li>
      </ul>
    </nav>
  </header>


  <section id="home">
    <div class="content">
      <h1>DAFTARKAN DIRI ANDA SEKARANG !</h1>
      <p>Silahkan klik daftar dibawah ini untuk melakukan pendaftaran<br>,kemudian ikuti langkah selanjutnya</p>
      <div>
        <br>
        <br>
        <a href="formdaftar.php" class="btn-daftar">Daftar</a>
  </section>

  <script src="script.js"></script>
</body>

</html>
