<?php
session_start();
include 'koneksi.php';
if ($_SESSION['stat_login'] != true) {
  echo '<script>window.location="login.php"</script>';
}
?>

<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title>Menu Admin</title>
  <link rel="stylesheet" href="css/data2.css">
</head>

<body>

  <input type="checkbox" id="check">

  <header>
    <label for="check">
      <img src="menu.png" width="20" id="sidebar_btn">
    </label>
    <div class="left_area">
      <h3>ACADEMY <span>THE RAID</span></h3>
    </div>

  </header>

  <div class="sidebar">

    <img src="rdd.png" class="profile_image2">
    <h4>Admin</h4>

    <a href="beranda.php"><img src="apps.png" width="20"><span>Beranda</span></a>
    <a href="data-peserta.php"><img src="users.png" width="20"><span>Data Peserta</span></a>
    <a href="index.php"><img src="exit.png" width="20"><span>Keluar</span></a>
  </div>


  <div class="content">
    <div class="box">
      <h2>Data Peserta</h2>
      <div class="btn-cetak">
        <a href="cetak-peserta.php" class="print_btn">Print</a>
      </div>
      <table class="table">
        <thead>
          <tr>
            <th>No</th>
            <th>ID Pendaftaran</th>
            <th>Nama</th>
            <th>Jenis Kelamin</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $no = 1;
          $list_peserta = mysqli_query($conn, "SELECT * FROM tb_pendaftaran");
          while ($row = mysqli_fetch_array($list_peserta)) {
            ?>
            <tr>
              <td><?php echo $no++ ?></td>
              <td>
                <?php echo $row['id_pendaftaran'] ?>
              </td>
              <td><?php echo $row['nm_peserta'] ?></td>
              <td>
                <?php echo $row['jk'] ?>
              </td>
              <td>
                <a href="detail-peserta.php?id=<?php echo $row['id_pendaftaran'] ?>" class="aksi_btn">Detail</a> ||
                <a href="hapus-peserta.php?id=<?php echo $row['id_pendaftaran'] ?>" class="aksi_btn"
                  onclick="return confirm('Anda yakin ingin menghapus data ini?')">Hapus</a>
              </td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>

</body>

</html>
