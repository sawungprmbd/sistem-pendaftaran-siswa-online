<?php
include 'koneksi.php';
?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>PSB Online</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>

<body>
	<section class="box-formulir">
		<div class="box">
			<h2>Pendaftaran Berhasil</h2>
			<h4>Kode Pendaftaran Anda Adalah <?php echo $_GET['id'] ?></h4>
			<br>
			<a href="cetakbukti.php?id=<?php echo $_GET['id'] ?>" class="btn-cetak">Cetak Bukti Daftar</a>
			<br>
			<br>
			<a href="index.php">kembali</a>
		</div>
	</section>

</body>

</html>
