<?php
session_start();
include 'koneksi.php';
if ($_SESSION['stat_login'] != true) {
  echo '<script>window.location="login.php"</script>';
}
?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title>Menu Admin</title>
  <link rel="stylesheet" href="css/data2.css">
</head>

<body>

  <input type="checkbox" id="check">
  <header>
    <label for="check">
      <img src="menu.png" width="20" id="sidebar_btn">
    </label>
    <div class="left_area">
      <h3>ACADEMY <span>THE RAID</span></h3>
    </div>

  </header>


  <div class="sidebar">

    <img src="rdd.png" class="profile_image">
    <h4>Admin</h4>

    <a href="beranda.php"><img src="apps.png" width="20"><span>Beranda</span></a>
    <a href="data-peserta.php"><img src="users.png" width="20"><span>Data Peserta</span></a>
    <a href="index.php"><img src="exit.png" width="20"><span>Keluar</span></a>
  </div>

  <section class="content">
    <div class="box">
      <h2>Beranda</h2>
      <h3>
        <?php echo $_SESSION['nama'] ?>, Selamat Datang Di PSB Online
      </h3>
    </div>
  </section>

</body>

</html>
