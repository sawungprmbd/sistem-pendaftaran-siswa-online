<?php
session_start();
include 'koneksi.php';
if ($_SESSION['stat_login'] != true) {
	echo '<script>window.location="login.php"</script>';
}

$peserta = mysqli_query($conn, "SELECT * FROM tb_pendaftaran WHERE id_pendaftaran = '" . $_GET['id'] . "' ");
$p = mysqli_fetch_object($peserta);
?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<title>Data Siswa</title>
	<link rel="stylesheet" href="css/data2.css">
</head>

<body>

	<input type="checkbox" id="check">

	<header>
		<label for="check">
			<img src="menu.png" width="20" id="sidebar_btn">
		</label>
		<div class="left_area">
			<h3>ACADEMY <span>THE RAID</span></h3>
		</div>

	</header>

	<div class="sidebar">

		<img src="rdd.png" class="profile_image2">
		<h4>Admin</h4>

		<a href="beranda.php"><img src="apps.png" width="20"><span>Beranda</span></a>
		<a href="data-peserta.php"><img src="users.png" width="20"><span>Data Peserta</span></a>
		<a href="index.php"><img src="exit.png" width="20"><span>Keluar</span></a>
	</div>




	<div class="content">
		<div class="box">
			<h2>Data Peserta</h2>
			<table class="table" border="1">
				<tr>
					<td>Kode Pendaftaran</td>
					<td>:</td>
					<td>
						<?php echo $p->id_pendaftaran ?>
					</td>
				</tr>
				<tr>
					<td>Tahun Ajaran</td>
					<td>:</td>
					<td><?php echo $p->th_ajaran ?></td>
				</tr>
				<tr>
					<td>Jurusan</td>
					<td>:</td>
					<td>
						<?php echo $p->jurusan ?>
					</td>
				</tr>
				<tr>
					<td>Nama Lengkap</td>
					<td>:</td>
					<td>
						<?php echo $p->nm_peserta ?>
					</td>
				</tr>
				<tr>
					<td>Tempat, Tanggal Lahir</td>
					<td>:</td>
					<td>
						<?php echo $p->tmp_lahir . ', ' . $p->tgl_lahir ?>
					</td>
				</tr>
				<tr>
					<td>Jenis Kelamin</td>
					<td>:</td>
					<td><?php echo $p->jk ?></td>
				</tr>
				<tr>
					<td>Agama</td>
					<td>:</td>
					<td>
						<?php echo $p->agama ?>
					</td>
				</tr>
				<tr>
					<td>Alamat</td>
					<td>:</td>
					<td>
						<?php echo $p->almt_peserta ?>
					</td>
				</tr>
			</table>

		</div>
	</div>

</body>

</html>
